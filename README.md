# Clash-Rules

Self Clash Rules / 自用 Clash 规则集

Because CFW end of support (the project was removed), so this rule sets is not for CFW.

由于 CFW（Clash For Windows）已停止更新，故不提供 CFW 相关配置。

All configs under this for Clash Verge only, Clash Verge Rev version must `>=1.7.2`.

以下所有配置适用于 Clash Verge，Clash Verge Rev 版本需 `>=v1.7.2`。

## Global Extend Script

The script's purpose is to create a new proxy group including HK/MC/TW nodes and another group for special services.

**You need to add subscribe link or proxy nodes first.**

Fill under these codes to the **Global Extend Script**:

```javascript
// Define main function (script entry)

function main(config, profileName) {
    config = replaceProxyName(config, '一元机场', 'Proxy');

    // repeat this code if had multiple profiles
    // config = replaceProxyName(config, 'AProfileProxyGroupName', 'Proxy');
    // config = replaceProxyName(config, 'BProfileProxyGroupName', 'Proxy');

    const hkMCTWProxies = getProxiesByRegex(config, /香港|HK|hk|澳门|MC|mc|台湾|TW|tw/);

    // HK nodes filtered
    const asiaFilteredProxies = getProxiesByRegex(config, /^(?!.*(香港|HK|hk|有效期)).*?$/);

    // MapleStory
    const mapleStoryProxies = getProxiesByRegex(config, /澳大利亚|AU|au|美国|US|us|英国|UK|uk|德国|DE|de/);

    const HK_MC_TW = {
        name: 'Auto-HK-MC-TW',
        type: 'url-test',
        url: 'https://www.gstatic.com/generate_204',
        interval: 60, // for delay test
        proxies: hkMCTWProxies
    };

    const BILIBILI = {
        name: 'BiliBili',
        type: 'select',
        proxies: [
            'DIRECT', 'Auto-HK-MC-TW'
        ]
    };

    const ASIA_FILTERED = {
        name: 'AsiaFiltered',
        type: 'url-test',
        url: 'https://www.gstatic.com/generate_204',
        interval: 60,
        proxies: asiaFilteredProxies
    };

    const MAPLESTORY = {
        name: 'MapleStory',
        type: 'url-test',
        url: 'https://www.gstatic.com/generate_204',
        interval: 120,
        proxies: mapleStoryProxies
    };

    config['proxy-groups'].push(HK_MC_TW, BILIBILI, ASIA_FILTERED, MAPLESTORY);

    return config;
}

/**
 * @param config
 * @param origin
 * @param target
 * 
 * @return Object
 */
function replaceProxyName(config, origin, target) {
    // replace proxy group name
    config['proxy-groups'].map((item) => item.name = item.name.replace(origin, target));

    // use JSON serialize for replace default rules proxy name
    config.rules = JSON.parse(JSON.stringify(config.rules).replaceAll(origin, target));

    return config;
}

/**
 * @param config
 * @param regex
 * 
 * @return Array
 */
function getProxiesByRegex(config, regex) {
    return config.proxies.filter((e) => regex.test(e.name)).map((e) => e.name);
}
```

## Global Extend Config

This config same as **Mixin** of CFW.

All config documents: [https://wiki.metacubex.one/config](https://wiki.metacubex.one/config)

So just do replace:

```yaml
# Profile Enhancement Merge Template for Clash Verge

keep-alive-interval: 60
keep-alive-idle: 60
tcp-concurrent: true

profile: 
  store-selected: false
  store-fake-ip: false

dns:
  enable: true
  ipv6: true
  listen: :53
  prefer-h3: true
  use-hosts: true
  use-system-hosts: true
  enhanced-mode: normal # fake ip config only for `fake-ip` mode
  fake-ip-range: 198.18.0.1/16
  fake-ip-filter:
    - +.stun.*.*
    - +.stun.*.*.*
    - +.stun.*.*.*.*
    - +.stun.*.*.*.*.*
    - +.stun.playstation.net
    - "*.*.xboxlive.com"
    - "*.msftncsi.com"
    - "*.msftconnecttest.com"
    - "*.n.n.srv.nintendo.net"
    - "xbox.*.*.microsoft.com"
    - WORKGROUP
  default-nameserver:
    - 114.114.114.114
    - 119.29.29.29
    - system
  nameserver:
    - 119.29.29.29
    - 114.114.114.114
    - 8.8.8.8
    - 9.9.9.9
    - 208.67.222.222
  proxy-server-nameserver:
    - 8.8.8.8
    - 9.9.9.9
    - 208.67.222.222
  nameserver-policy:
    "geosite:cn,private":
      - 114.114.114.114
      - 119.29.29.29
      - system
  fallback:
    - 8.8.8.8
    - 9.9.9.9
    - 208.67.222.222

sniffer:
  enable: true
  override-destination: true
  force-dns-mapping: true
  parse-pure-ip: true
  sniff: 
    HTTP: 
      ports:
        - 80
        - 8080
    TLS:
      ports:
        - 443
        - 8443
    QUIC:
      ports:
        - 443
        - 8443

rule-providers:
  custom-rule-game:
    type: http
    behavior: domain
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-game.txt"
    interval: 7200
    
  custom-rule-video:
    type: http
    behavior: domain
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-video.txt"
    interval: 7200
    
  custom-rule-direct:
    type: http
    behavior: domain
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-direct.txt"
    interval: 7200

  custom-rule-direct-app:
    type: http
    behavior: classical
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-direct-app.txt"
    interval: 7200

  custom-rule-direct-ip:
    type: http
    behavior: ipcidr
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-direct-ip.txt"
    interval: 7200
    
  custom-rule-proxy:
    type: http
    behavior: domain
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-proxy.txt"
    interval: 7200

  custom-rule-proxy-ip:
    type: http
    behavior: ipcidr
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-proxy-ip.txt"
    interval: 7200

  custom-rule-proxy-not-asia:
    type: http
    behavior: domain
    url: "https://gitlab.com/maxsky/Clash-Rules/-/raw/master/custom-rule-proxy-not-asia.txt"
    interval: 7200

  private:
    type: http
    behavior: domain
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/private.txt"
    interval: 7200

  direct:
    type: http
    behavior: domain
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/direct.txt"
    interval: 7200

  reject:
    type: http
    behavior: domain
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/reject.txt"
    interval: 7200

  gfw:
    type: http
    behavior: domain
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/gfw.txt"
    interval: 7200

  telegramcidr:
    type: http
    behavior: ipcidr
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/telegramcidr.txt"
    interval: 7200
    
  applications:
    type: http
    behavior: classical
    url: "https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/applications.txt"
    interval: 7200

rules:
  - RULE-SET,private,DIRECT
  - RULE-SET,custom-rule-direct-app,DIRECT
  # direct rule set need test by yourself
  #- RULE-SET,custom-rule-direct,DIRECT
  #- RULE-SET,custom-rule-direct-ip,DIRECT
  - RULE-SET,custom-rule-game,Proxy
  - RULE-SET,custom-rule-proxy,Proxy
  - RULE-SET,custom-rule-proxy-ip,Proxy
  # this rule for the video only of HK/MC/TW from BiliBili
  - RULE-SET,custom-rule-video,BiliBili
  - RULE-SET,custom-rule-proxy-not-asia,AsiaFiltered
  - RULE-SET,direct,DIRECT
  # reject some reports or ads connection
  - RULE-SET,reject,REJECT
  - GEOIP,CN,DIRECT
  - RULE-SET,gfw,Proxy
  - RULE-SET,telegramcidr,Proxy
  - RULE-SET,applications,DIRECT
  # If you know how to add custom rules, you can set MATCH to DIRECT
  - MATCH,Proxy
```

## System Proxy Bypass

Open **Settings** menu and enable `System Proxy` setting, then click ⚙ icon to fill under contents to **Proxy Bypass**:

```
localhost;127.*;192.168.*;10.*;172.16.*;172.17.*;172.18.*;172.19.*;172.20.*;172.21.*;172.22.*;172.23.*;172.24.*;172.25.*;172.26.*;172.27.*;172.28.*;172.29.*;172.30.*;172.31.*;*.126.net;*.163.com;*.1drv.com;*.akamaihd.net;*.akamaized.net;*.apple.com.cn;*.bilivideo.cn;*.cm.steampowered.com;*.delivery.mp.microsoft.com;*.gvt1-cn.com;*.icloud.com.cn;*.myqcloud.com;*.netease.com;*.steamcontent.com;*.steamserver.net;*.xunlei.com;cdn.steamstatic.com;dl.google.com;dl.steam.clngaa.com;download.visualstudio.microsoft.com;gstore.val.manlaxy.com;ocsp2.apple.com;st.dl.eccdnx.com;swcdn.apple.com;updates.cdn-apple.com;xz.pphimalayanrt.com;<local>
```

**macOS** need use comma as separator:

```
localhost,127.*,192.168.*,10.*,172.16.*,172.17.*,172.18.*,172.19.*,172.20.*,172.21.*,172.22.*,172.23.*,172.24.*,172.25.*,172.26.*,172.27.*,172.28.*,172.29.*,172.30.*,172.31.*,*.126.net,*.163.com,*.1drv.com,*.akamaihd.net,*.akamaized.net,*.apple.com.cn,*.bilivideo.cn,*.cm.steampowered.com,*.delivery.mp.microsoft.com,*.gvt1-cn.com,*.icloud.com.cn,*.myqcloud.com,*.netease.com,*.steamcontent.com,*.steamserver.net,*.xunlei.com,cdn.steamstatic.com,dl.google.com,dl.steam.clngaa.com,download.visualstudio.microsoft.com,gstore.val.manlaxy.com,ocsp2.apple.com,st.dl.eccdnx.com,swcdn.apple.com,updates.cdn-apple.com,xz.pphimalayanrt.com,<local>
```

## API for Desktop/Mobile Clients

This API is useful to supplement the proxy rules of the default profile.

The converted result supports Clash Verge (PC/macOS/Linux) and Clash Meta (Android) clients. (No need above configs.)

API: `https://tool.maxsky.cc/api/utils/clash/profile/convert`, Method: `GET`

Parameters: (Required if no default value)

| name | type | default |description |
| ------ | ------ | ------ | ----- |
| type | `string` | `yaml` | Accept `yaml` or `trojan`. |
| match | `string` | `proxy` | Accept `proxy` or `direct`; this parameter is specific to a **default** behavior if the domain is not in all rules. |
| proxy_name | `string` | `Proxy` | Only for `yaml` type. Please use group name of `proxy-groups` node with `type` is `select` in your profile. All proxy names will be changed to `Proxy` finally if you do not use this parameter. |
| sniffer | `int` | `0` | To enable sniffer feature, set `1` if you need. |
| url | `string` | - | Subscribe URL, recommend doing URL encode if URL has query parameters. |

API use example:

1. `https://tool.maxsky.cc/api/utils/clash/profile/convert?type=trojan&match=proxy&url=http://subscribe.url/aaa/bbb/ccc?secret=123456789`
2. `https://tool.maxsky.cc/api/utils/clash/profile/convert?type=yaml&match=direct&proxy_name=代理组名&url=http://subscribe.url/aaa/bbb/ccc?secret=123456789`

**Use this concat URL as the subscribe URL in clients.**

**This API is deployed to a free server, if you can't use maybe the server was shutdown, just try again after 1 minute and the server will start.**

**This API does not record any data, if you don't trust this, please do not use it.**

### 一元示例

由于一元默认的订阅链接未携带 `flag` 参数，使用本接口进行转换时 **强烈建议** 在订阅链接中添加 `flag` 参数。

拼接结果示例：`https://tool.maxsky.cc/api/utils/clash/profile/convert?match=direct&proxy_name=一元机场&url=http%3A%2F%2Fsubscribe.url%2Faaa%2Fbbb%2Fccc%3Ftoken%3D123456789%26flag%3Dclash`

注意这里的 `proxy_name` 必传，值为 `一元机场`。

如果未传递 `flag` 参数，则订阅链接内容为 `trojan` 协议链接，此时需使用 `type` 参数并传入值 `trojan`。而加上 `flag=clash` 后结果为 `yaml` 配置，可忽略 `type` 参数。

**加上 `flag` 参数有助于获取已使用流量信息**。

**最后需将订阅地址进行 URL Encode 编码操作：**

原：`http://subscribe.url/aaa/bbb/ccc?token=123456789&flag=clash`

编码后：`http%3A%2F%2Fsubscribe.url%2Faaa%2Fbbb%2Fccc%3Ftoken%3D123456789%26flag%3Dclash`

在线编码工具：[URL编码 / URL解码](https://tool.box3.cn/urlencode-urldecode.html)

Some default params can be ignored.

    PS: If your subscribe url contents is multiple line trojan links, use trojan type. (Support Base64 encoded subscribe contents.)

More parameters will be added in the future.
